package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.exception.VetNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class VetServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(VetServiceTest.class);
	
	@Autowired
    private VetService vetService;

	@Test
	public void testFindVeterinarioById() {
		long ID = 5;
		String NOMBRE = "Henry";
		Vet vet = null;
		
		try {
			vet = vetService.findById(ID); //hace la busqueda
			
		} catch (VetNotFoundException e) {
			fail(e.getMessage());
		}
		
		logger.info(">_RESULTADO (ID) -> " + vet.getFirstName()); //muestra los resultados
		assertEquals(NOMBRE, vet.getFirstName()); //compara lo esperado con el resultado
	}
	
	@Test
	public void testFindVeterinarioByFirstName() {
		String NOMBRE = "James";
		int SIZE_EXPECTED = 1;

		List<Vet> vets = vetService.findByFirstName(NOMBRE);

		logger.info(">_RESULTADOS (NOM) -> " + vets.get(0).getFirstName()); //muestra los resultados
		assertEquals(SIZE_EXPECTED, vets.size());
	}
	
	@Test
	public void testUpdateVeterinario() { 
		String NOMBRE = "Jamutaq";
		String APELLIDO = "Ortega";
		long create_id = -1;

		//nuevos datos
		String NUEVO_NOMBRE = "Jamutaq_nuevo";
		String NUEVO_APELLIDO = "Ortega_nuevo";

		//crea un objeto veterinario con los datos
		Vet vet = new Vet(NOMBRE, APELLIDO); 
		logger.info(">_OBJETO VETERINARIO CREADO -> " + vet.getFirstName());
		
		//inserta en la bd el veterinario y retorna el objeto
		Vet vetBD = vetService.create(vet); 
		logger.info(">_OBJETO VETERINARIO INSERTADO -> " + vetBD.getFirstName());

		create_id = vetBD.getId(); //obtiene el ID de lo que se inserto en la BD

		//se asignan los nuevos datos al objeto veterinarioBD
		vetBD.setLastName(NUEVO_APELLIDO);
		vetBD.setFirstName(NUEVO_NOMBRE);

		//ejecuta el update, con los nuevos datos asignados
		Vet upgradeVet = vetService.update(vetBD);
		logger.info(">_OBJETO VETERINARIO ACTUALIZADO -> " + upgradeVet.getFirstName());

		//compara los resultados que estan en la BD con lo que se asignó
		assertThat(create_id).isNotNull();
		assertEquals(create_id, upgradeVet.getId());
		assertEquals(NUEVO_APELLIDO, upgradeVet.getLastName());
		assertEquals(NUEVO_NOMBRE, upgradeVet.getFirstName());
	}
	

	
	//TESTS DE CRUD
		@Test
		public void testCreateVeterinario() { //AUTOR: KRISS GALIANO
			String NOMBRE = "Kriss";
			String APELLIDO = "Galiano";

			Vet vet = new Vet(NOMBRE, APELLIDO); //crea un objeto
			vet = vetService.create(vet); //crea el veterinario con los datos pasados
			logger.info(">_VETERINARIO INSERTADO -> " + vet.getFirstName());

			//hace las compaaciones
			assertThat(vet.getId()).isNotNull();
			assertEquals(NOMBRE, vet.getFirstName());
			assertEquals(APELLIDO, vet.getLastName());
		}

	@Test
	public void testDeleteVeterinario() { //AUTOR: Kriss Galiano
		String NOMBRE = "Kriss";
		String APELLIDO = "Galiano";

		//crea un nuevo objeto con los datos
		Vet vet = new Vet(NOMBRE, APELLIDO);
		vet = vetService.create(vet); //inserta en la BD
		logger.info(">_VETERINARIO INSERTADO (D) -> " + vet.getFirstName());

		try { //elimina el objeto insertado por su id
			vetService.delete(vet.getId());
			logger.info(">_VETERINARIO ELIMINADO -> " + vet.getFirstName());
			
		} catch (VetNotFoundException e) {
			fail(e.getMessage());
		}
			
		try { //busca en la bd el objeto eliminado
			vetService.findById(vet.getId());
			assertTrue(false); //no debe de devolver ningun dato
			
		} catch (VetNotFoundException e) {
			assertTrue(true);
		} 				

	}

}
