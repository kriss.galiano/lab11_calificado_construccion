package com.tecsup.petclinic.service;

import java.util.List;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.exception.VetNotFoundException;

public interface VetService {

	//Kriss Galiano
	Vet create(Vet vet);
	void delete(Long id) throws VetNotFoundException;
	
	//Jamutaq Ortega 
	Vet update(Vet vet);
    Vet findById(long id) throws VetNotFoundException;
    List<Vet> findByFirstName(String firstName);
    Iterable<Vet> findAll();
	
	
}
