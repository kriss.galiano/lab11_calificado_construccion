package com.tecsup.petclinic.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.domain.VetRepository;
import com.tecsup.petclinic.exception.VetNotFoundException;

@Service
public class VetServiceImpl implements VetService{

private static final Logger logger = LoggerFactory.getLogger(VetServiceImpl.class);
	
	@Autowired
	VetRepository repo;
	
	//Jamutaq Ortega
    @Override
    public Vet update(Vet vet) {
        return repo.save(vet);
    }
    @Override
    public Vet findById(long id) throws VetNotFoundException {
        Optional<Vet> vet = repo.findById(id);

        if (!vet.isPresent()) //si no se encuentra
            throw new VetNotFoundException("ERROR: NO SE ENCONTRO EL VETERINARIO!");
            
        return vet.get(); //si se encontro, que devuelva el veterinario
    }
    @Override
    public List<Vet> findByFirstName(String nombre) {
        List<Vet> vets = repo.findByFirstName(nombre);
        vets.stream().forEach(veterinario -> logger.info("" + veterinario));

        return vets;
    }
    @Override
    public Iterable<Vet> findAll() {
        return repo.findAll(); 
    }
	@Override
	public Vet create(Vet vet) {
		return repo.save(vet);
	}
	@Override
	public void delete(Long id) throws VetNotFoundException {
		Vet vet = findById(id);
		repo.delete(vet);
		
	}
}
